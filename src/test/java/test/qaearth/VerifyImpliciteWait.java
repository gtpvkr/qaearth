package test.qaearth;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class VerifyImpliciteWait {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://qaearth.blogspot.in/p/testing-examples.html");
		// Specify implicit wait of 30 seconds
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// No login id is present on Webpage so this will fail our script.
		driver.findElement(By.id("login")).sendKeys("Selenium");
	}

}
