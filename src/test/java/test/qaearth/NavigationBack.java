package test.qaearth;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class NavigationBack {
	@Test
	public void navigationToURLExample() {
		FirefoxDriver driver = new FirefoxDriver();
		String URL = "http://www.facebook.com";
		driver.navigate().to(URL);
		driver.findElement(By.linkText("Forgot your password?")).click();
		driver.navigate().back();
	}
}