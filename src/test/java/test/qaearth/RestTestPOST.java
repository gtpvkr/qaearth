package test.qaearth;

import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import java.util.List;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestTestPOST {
	
@Test
public void httpPost() {

	// Initializing Rest API's URL
	String APIUrl = "http://{API URL}";

	// Initializing payload or API body
	String APIBody = "{API Body}"; // e.g.-
									// "{\"key1\":\"value1\",\"key2\":\"value2\"}"

	// Building request using requestSpecBuilder
	RequestSpecBuilder builder = new RequestSpecBuilder();

	// Setting API's body
	builder.setBody(APIBody);

	// Setting content type as application/json or application/xml
	builder.setContentType("application/json; charset=UTF-8");

	RequestSpecification requestSpec = builder.build();

	// Making post request with authentication, leave blank in case there
	// are no credentials- basic("","")
	Response response = given().authentication().preemptive().basic("username", "password").spec(requestSpec).when()
			.post(APIUrl);
	// Fetching param
	List alpha3Code = response.then().contentType(ContentType.JSON).extract().path("key");

	// Asserting that capital of Norway is Oslo
	Assert.assertEquals(alpha3Code.get(0), "Expected value");

}
}
