package test.qaearth;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class URLExample {
	@Test
	public void navigationToURLExample() {
		FirefoxDriver driver = new FirefoxDriver();
		driver.navigate().to("http://www.google.com");
	}
}