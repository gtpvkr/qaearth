package test.qaearth;

import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TestMultipleWindows {
	@Test
	public void TestNewWindow() {
		// Open browser
		System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// Maximize browser
		driver.manage().window().maximize();
		// Load app
		driver.get("https://qaearth.blogspot.in/p/testing-examples.html");
		driver.findElement(By.xpath("//*[text()='Open Window 1']")).click();

		// This will return the number of windows opened by Webdriver and will
		// return Set of Strings
		Set<String> windowHandles = driver.getWindowHandles();
		// Now we will iterate using Iterator
		Iterator<String> it = windowHandles.iterator();
		// It will return the parent window name as a String
		String parentBrowser = it.next();
		// It will return the child window name as a String
		String childBrowser = it.next();
		// switch to child window
		driver.switchTo().window(childBrowser);
		// Once actions done on child window can use close() method to close
		// current window
		driver.close();
		// switch to parent window to do rest activities
		driver.switchTo().window(parentBrowser);

	}
}