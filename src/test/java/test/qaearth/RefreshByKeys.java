package test.qaearth;

import org.openqa.selenium.Keys;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class RefreshByKeys {
 @Test
 public void refresh() throws InterruptedException {
  FirefoxDriver driver = new FirefoxDriver();
  driver.navigate().to("http://google.com");
  Actions actions = new Actions(driver);
  actions.keyDown(Keys.CONTROL).sendKeys(Keys.F5).perform();
 }
}