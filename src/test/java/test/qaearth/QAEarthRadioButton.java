package test.qaearth;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class QAEarthRadioButton {
	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.gecko.driver", "D:\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://qaearth.blogspot.in/p/testing-examples.html");
		WebElement male_radio_button = driver.findElement(By.id("radio1"));
		boolean status = male_radio_button.isDisplayed();
		System.out.println("Male radio button is Displayed >>" + status);
		boolean enabled_status = male_radio_button.isEnabled();
		System.out.println("Male radio button is Enabled >>" + enabled_status);
		boolean selected_status = male_radio_button.isSelected();
		System.out.println("Male radio button is Selected >>" + selected_status);
		male_radio_button.click();
		boolean selected_status_new = male_radio_button.isSelected();
		System.out.println("Male radio button is Selected >>" + selected_status_new);
	}
}