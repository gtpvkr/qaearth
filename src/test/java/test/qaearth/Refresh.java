package test.qaearth;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Refresh {
	@Test
	public void refresh() throws InterruptedException {
		FirefoxDriver driver = new FirefoxDriver();
		String URL = "http://www.facebook.com";
		driver.get(URL);
		driver.findElement(By.linkText("Forgot your password?")).click();
		driver.findElement(By.id("identify_email")).sendKeys("sample@email.com");
		driver.navigate().refresh();
	}
}