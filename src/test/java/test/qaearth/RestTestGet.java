package test.qaearth;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

import java.util.List;

public class RestTestGet {

	@Test
	public void getRequestFindCapital() {

		// make get request to fetch capital of norway
		Response resp = given().get("API URl");

		// Fetching response in string
		System.out.println(resp.asString());

		// Fetching param
		List alpha3Code = resp.then().contentType(ContentType.JSON).extract().path("key");

		// Asserting that capital of Norway is Oslo
		Assert.assertEquals(alpha3Code.get(0), "Expected value");
	}

}