package test.qaearth;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxLaunchWithOutDriver {
	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://qaearth.blogspot.in/p/testing-examples.html");
		driver.quit();
	}
}