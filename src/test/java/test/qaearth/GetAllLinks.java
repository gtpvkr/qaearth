package test.qaearth;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GetAllLinks {

	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver", "Drivers/geckodriver.exe");
		WebDriver driver = new FirefoxDriver();

		// Launching sample website
		driver.get("http://www.qaearth.com/p/testing-examples.html");
		driver.manage().window().maximize();

		// Get list of web-elements with tagName - a
		List<WebElement> allLinks = driver.findElements(By.tagName("a"));

		// Traversing through the list and printing its text along with link
		// address
		for (WebElement link : allLinks) {
			System.out.println(link.getText() + " - " + link.getAttribute("href"));
		}

		// Commenting driver.quit() for user to verify the links printed
		// driver.quit();
	}

}