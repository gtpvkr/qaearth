package test.qaearth;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PopupsHandling {
	public static void main(String a[]) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "Drivers/geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://qaearth.blogspot.in/p/testing-examples.html");
		driver.findElement(By.cssSelector("#alert")).click();
		Thread.sleep(2000);
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		alert.sendKeys("Krishna");
		alert.accept();
		// alert.dismiss();
	}
}