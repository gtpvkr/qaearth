package test.qaearth;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
public class Navigationforward {
 @Test
 public void navigationforward() throws InterruptedException {
  FirefoxDriver driver = new FirefoxDriver();
  String URL = "http://www.facebook.com";
  driver.navigate().to(URL);
  driver.findElement(By.linkText("Forgot your password?")).click();
  driver.navigate().back();
  Thread.sleep(1000);
  driver.navigate().forward();
 }
}